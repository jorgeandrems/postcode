#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: New Employer",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Register New Employer");
	if (!param) {
		employerForm();
	}
	else {
		employerFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
