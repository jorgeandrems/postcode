#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;
use CGI::Session;

$session = new CGI::Session();

print $session->header();
print	start_html(	-title=>"Unijobs.com.au: Login",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Login");
	if (!param) {
		loginForm();
	}
	else {
		loginFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
