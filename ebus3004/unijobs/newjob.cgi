#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: New Job",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Register New Job");
	if (!param) {
		jobForm();
	}
	else {
		jobFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
