#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Search for Jobs",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Search For Jobs");
	if (!param) {
		searchJobForm();
	}
	else {
		searchJobFormErrors();
	}
print	pfooter(),
	"</div>",
