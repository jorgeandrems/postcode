--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.student DROP CONSTRAINT student_pkey;
DROP TABLE public.student;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: student; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE student (
    studentid serial NOT NULL,
    firstname character varying(30) NOT NULL,
    lastname character varying(30) NOT NULL,
    email character varying(60),
    dob date,
    phonenumber character varying(15) NOT NULL,
    "password" character varying(32) NOT NULL,
    streetaddress character varying(60),
    postcode numeric(4,0),
    state character varying(3),
    suburb character varying(20),
    gpa numeric,
    degree character varying(60) NOT NULL,
    major character varying(60),
    yearcommenced numeric(4,0),
    completionyear numeric(4,0),
    keywords character varying(512),
    availablefrom date,
    availabletill date,
    CONSTRAINT gparange CHECK (((gpa >= (0)::numeric) AND (gpa <= (100)::numeric)))
);


--
-- Name: student_studentid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('student', 'studentid'), 5, true);


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY student (studentid, firstname, lastname, email, dob, phonenumber, "password", streetaddress, postcode, state, suburb, gpa, degree, major, yearcommenced, completionyear, keywords, availablefrom, availabletill) FROM stdin;
2	Joe	Smith	joe@joe.com	1990-01-01	333	a	32 address street	4444	NSW	sdf	55	arts	painting	2000	2006		2006-10-25	2006-10-25
3	Daniel	Carlier	dcar8770@mail.usyd.edu.au	1985-07-18	0	b	3	2000	NSW	Terrigal	95	Software Engineering	Ebus	2000	2006		2006-10-25	2006-10-25
4	Daniel	Carlier	dcar8770@mail.usyd.edu.au	1985-07-18	0	b	3	2000	NSW	Terrigal	95	Software Engineering	Ebus	2000	2006		2006-10-25	2006-10-25
5	bill	smith	asdasd@asdasd.com	1986-06-04	042304230	a	wewe	4444	NSW	somewhere	99	science	ebus	2004	2012	ebus bill	2006-10-27	2007-02-25
1	Ivan	Kruchkoff	ivan.kk@gmail.com	1990-01-01	042304230	200566238e4c645810c7a57bc1adc89a	56 Amundsen street	2560	NSW	Leumeah	98	BCST	Networks and Info Systems	2000	2006		2006-10-25	2006-10-25
\.


--
-- Name: student_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_pkey PRIMARY KEY (studentid);


--
-- PostgreSQL database dump complete
--

