DROP TABLE subjectGrade;
DROP TABLE student;
DROP TABLE job;
DROP TABLE employer;
DROP TABLE resetPassword;


CREATE TABLE employer (
    employerID serial,
    password varchar(32) NOT NULL,  
    company varchar(30) NOT NULL, 
    contactName varchar(40) NOT NULL, 
    phoneNumber varchar(15) NOT NULL,
    email varchar(60), 
    companyPosition varchar(40),
    PRIMARY KEY (employerID)
);

CREATE TABLE job (
    jobID serial,
    jobTitle varchar(60) NOT NULL,
    description varchar(1024) NOT NULL,
    dateListed date DEFAULT ('now'::text)::date,
    status varchar(60),
    fromPayBracket integer,
    toPayBracket integer,
    startDate date,
    endDate date,
    employerID varchar(30), 
    streetAddress varchar(60),
    postCode numeric(4), 
    suburb varchar(25),
    state varchar(3),
    PRIMARY KEY (jobID),
    FOREIGN KEY (employerID) REFERENCES employer (employerID)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
);

CREATE TABLE student (
    studentID serial,
    firstName varchar(30) NOT NULL,
    lastName varchar(30) NOT NULL,
    email varchar(60),
    dob date,
    phoneNumber varchar(15) NOT NULL,
    password varchar(32) NOT NULL,  
    streetAddress varchar(60),
    postCode numeric(4),
    state varchar(3),
    suburb varchar(20),
    gpa numeric,
    degree varchar(60) NOT NULL,
    major varchar(60),
    yearCommenced numeric(4), 
    completionYear numeric(4),
    keywords varchar(512),  -- csv?
    availableFrom date, 
    availableTill date,
    PRIMARY KEY (studentID),
    CONSTRAINT gpaRange CHECK (gpa >= 0 and gpa <= 100)
);

CREATE TABLE subjectGrade (
    gradeID serial NOT NULL,
    studentID serial NOT NULL,
    subject varchar(40) NOT NULL,
    grade numeric NOT NULL,
    year numeric(4) NOT NULL,
    semester numeric(1), -- either 1 or 2
    PRIMARY KEY (gradeID),
    FOREIGN KEY (studentID) REFERENCES student (studentID)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    CONSTRAINT gradeRange CHECK (grade >= 0 AND grade <= 100)
);


create TABLE resetPassword (id varchar(60) NOT NULL, resetcode varchar(32), userType varchar(1));

