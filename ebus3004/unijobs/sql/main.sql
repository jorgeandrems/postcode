--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.employer DROP CONSTRAINT employer_pkey;
DROP TABLE public.employer;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: employer; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE employer (
    employerid serial NOT NULL,
    "password" character varying(32) NOT NULL,
    company character varying(30) NOT NULL,
    contactname character varying(40) NOT NULL,
    phonenumber character varying(15) NOT NULL,
    email character varying(60),
    companyposition character varying(40)
);


--
-- Name: employer_employerid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('employer', 'employerid'), 3, true);


--
-- Data for Name: employer; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY employer (employerid, "password", company, contactname, phonenumber, email, companyposition) FROM stdin;
1	a	McDonalds	Ronald McDonald	041111111111111	ronald@mcdonald.com	CEO
2	b	Microsoft	Bill	8	bill@g.com	VP
3	b	Microsoft	Bill	8	bill@g.com	VP
\.


--
-- Name: employer_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY employer
    ADD CONSTRAINT employer_pkey PRIMARY KEY (employerid);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.job DROP CONSTRAINT job_employerid_fkey;
ALTER TABLE ONLY public.job DROP CONSTRAINT job_pkey;
DROP TABLE public.job;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: job; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE job (
    jobid serial NOT NULL,
    jobtitle character varying(60) NOT NULL,
    description character varying(1024) NOT NULL,
    datelisted date DEFAULT ('now'::text)::date,
    status character varying(60),
    frompaybracket integer,
    topaybracket integer,
    startdate date,
    enddate date,
    employerid character varying(30),
    streetaddress character varying(60),
    postcode numeric(4,0),
    suburb character varying(25),
    state character varying(3)
);


--
-- Name: job_jobid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('job', 'jobid'), 2, true);


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY job (jobid, jobtitle, description, datelisted, status, frompaybracket, topaybracket, startdate, enddate, employerid, streetaddress, postcode, suburb, state) FROM stdin;
1	President	President of our company.	2006-10-25	Hiring	6000	30000	2006-10-25	2006-12-25	1	1 Company street	2000	somewhere	NSW
2	Vice President	Executive Vice President of our company.	2006-10-25	Hiring	12	100	2006-10-28	2007-12-25	1	1 Company street	2000	somewhere	NSW
\.


--
-- Name: job_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (jobid);


--
-- Name: job_employerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivan
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_employerid_fkey FOREIGN KEY (employerid) REFERENCES employer(employerid) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP TABLE public.resetpassword;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: resetpassword; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE resetpassword (
    id character varying(60) NOT NULL,
    resetcode character varying(32),
    usertype character varying(1)
);


--
-- Data for Name: resetpassword; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY resetpassword (id, resetcode, usertype) FROM stdin;
1	a34e44e8482ffd4dbea0b49f7a812dba	E
\.


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.student DROP CONSTRAINT student_pkey;
DROP TABLE public.student;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: student; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE student (
    studentid serial NOT NULL,
    firstname character varying(30) NOT NULL,
    lastname character varying(30) NOT NULL,
    email character varying(60),
    dob date,
    phonenumber character varying(15) NOT NULL,
    "password" character varying(32) NOT NULL,
    streetaddress character varying(60),
    postcode numeric(4,0),
    state character varying(3),
    suburb character varying(20),
    gpa numeric,
    degree character varying(60) NOT NULL,
    major character varying(60),
    yearcommenced numeric(4,0),
    completionyear numeric(4,0),
    keywords character varying(512),
    availablefrom date,
    availabletill date,
    CONSTRAINT gparange CHECK (((gpa >= (0)::numeric) AND (gpa <= (100)::numeric)))
);


--
-- Name: student_studentid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('student', 'studentid'), 5, true);


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY student (studentid, firstname, lastname, email, dob, phonenumber, "password", streetaddress, postcode, state, suburb, gpa, degree, major, yearcommenced, completionyear, keywords, availablefrom, availabletill) FROM stdin;
2	Joe	Smith	joe@joe.com	1990-01-01	333	a	32 address street	4444	NSW	sdf	55	arts	painting	2000	2006		2006-10-25	2006-10-25
3	Daniel	Carlier	dcar8770@mail.usyd.edu.au	1985-07-18	0	b	3	2000	NSW	Terrigal	95	Software Engineering	Ebus	2000	2006		2006-10-25	2006-10-25
4	Daniel	Carlier	dcar8770@mail.usyd.edu.au	1985-07-18	0	b	3	2000	NSW	Terrigal	95	Software Engineering	Ebus	2000	2006		2006-10-25	2006-10-25
5	bill	smith	asdasd@asdasd.com	1986-06-04	042304230	a	wewe	4444	NSW	somewhere	99	science	ebus	2004	2012	ebus bill	2006-10-27	2007-02-25
1	Ivan	Kruchkoff	ivan.kk@gmail.com	1990-01-01	042304230	200566238e4c645810c7a57bc1adc89a	56 Amundsen street	2560	NSW	Leumeah	98	BCST	Networks and Info Systems	2000	2006		2006-10-25	2006-10-25
\.


--
-- Name: student_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_pkey PRIMARY KEY (studentid);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.subjectgrade DROP CONSTRAINT subjectgrade_studentid_fkey;
ALTER TABLE ONLY public.subjectgrade DROP CONSTRAINT subjectgrade_pkey;
DROP TABLE public.subjectgrade;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: subjectgrade; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE subjectgrade (
    gradeid serial NOT NULL,
    studentid serial NOT NULL,
    subject character varying(40) NOT NULL,
    grade numeric NOT NULL,
    "year" numeric(4,0) NOT NULL,
    semester numeric(1,0),
    CONSTRAINT graderange CHECK (((grade >= (0)::numeric) AND (grade <= (100)::numeric)))
);


--
-- Name: subjectgrade_gradeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('subjectgrade', 'gradeid'), 1, true);


--
-- Name: subjectgrade_studentid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('subjectgrade', 'studentid'), 1, false);


--
-- Data for Name: subjectgrade; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY subjectgrade (gradeid, studentid, subject, grade, "year", semester) FROM stdin;
1	1	ebus3004 designing busses	89	2006	1
\.


--
-- Name: subjectgrade_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY subjectgrade
    ADD CONSTRAINT subjectgrade_pkey PRIMARY KEY (gradeid);


--
-- Name: subjectgrade_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivan
--

ALTER TABLE ONLY subjectgrade
    ADD CONSTRAINT subjectgrade_studentid_fkey FOREIGN KEY (studentid) REFERENCES student(studentid) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

