#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use DBI;
print 	header,
	start_html(	-title=>"View books",
			-style=>'/~ivan/BookShelf/bookshelf.css'); 
	require 'menu.cgi';
print	"<div class='site'>";
print	h1('View books');
	showBooks();
print	"</div>";
print	end_html;

sub showBooks() {
	$dbh = DBI->connect('dbi:Pg:dbname=bookdb;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT * FROM bookshelf";
	$sth = $dbh->prepare($query);
	$sth->execute;
	print 	'<table >',
		'<caption>Books</caption>',
		Tr({-align=>center,-valign=>top},
		[
			th(['Title','Author','ISBN', 'Description', 'Comment', 'Rating', 'Price', 'Quantity']),
		]
 	);
	while (@book = $sth->fetchrow)
	{
		#foreach $entry (@book) {
		#$entry = escapeHTML($entry);
		#}

		print "<tr align='center' valign='top' >".td(["<a href='update?id=$book[0]'>$book[1]</a>", "$book[2]", "$book[3]", "$book[4]", "$book[5]", "$book[6]", "\$$book[7]", "$book[8]"])."</tr>";
		#print "<tr align='center' valign='top' >".td(["<a href='update/$book[0]'>$book[1]</a>", "$book[2]", "$book[3]", "$book[4]", "$book[5]", "$book[6]", "\$$book[7]", "$book[8]"])."</tr>";
	}
	$dbh->disconnect();
	print '</table>';
	print p("Click on a title to modify the book or to delete it.");
}
