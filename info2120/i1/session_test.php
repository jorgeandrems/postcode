<?php session_start(); ?>

<html>
<head>
<title>
<?php
    $auth = false;

    if (isset($_REQUEST['_submit_check'])) {
       $_SESSION['user'] = $_REQUEST['login'];
       $_SESSION['id']   = $_REQUEST['login'] . 4711; 
    }

    if (isset($_REQUEST['logout'])) {
        session_destroy();
        unset($_SESSION['id']);
        echo "Logged out ... ";
    }

    if (isset($_SESSION['id'])) {
        echo "Welcome back: " . $_SESSION['id'];
        $auth = true;
    } else {
        echo "Please log in";
    }

?>
</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">

<?php 
        if( !$auth ) 
        {
?>
          <!-- The following will only be displayed if NOT logged in -->
           Login : <input type="Text" name="login" value="">
           <input type="submit" value="submit" name="submit"><br/>
           <input type="hidden" name="_submit_check" value="1"/>

<?php    
         } else { 
?>

            <!-- The following will only be displayed if logged in -->
           Hello <?php echo $_SESSION['user'] ?><br/>
           You are logged in - session id is: <?php echo $_SESSION['id'] ?>
           
           <p>
           Feel free to follow <a href="session_test2.php">this link</a>.
           </p>
             
           <p><input type="submit" value="logout" name="logout"></p>

<?php
         }
?>
   </form>
</body>
</html>
