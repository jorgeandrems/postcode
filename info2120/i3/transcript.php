<? require "inc/loggedinheader.php"; ?>
<h1>Student Transcript</h1>
<table>
	<caption>Transcript for <? echo $_SESSION['user'] ." (" .$_SESSION['id'] .")";?></caption>
	<tr>
		<th>Course Code</th>
		<th>Course Name</th>
		<th>Semester</th>
		<th>Year</th>
		<th>Grade</th>
	</tr>

<?php
  require "inc/db.php";
  if ($conn=oci_connect($dbUser, $dbPass, $db)) {
     // List all courses
     $id = $_SESSION['id'];
     $stmt = oci_parse($conn, "SELECT COURSE.CRSCODE, COURSE.CRSNAME, TRANSCRIPT.SEMESTER, TRANSCRIPT.YEAR, TRANSCRIPT.GRADE FROM COURSE, TRANSCRIPT WHERE TRANSCRIPT.STUDID = '$id' AND TRANSCRIPT.CRSCODE = COURSE.CRSCODE ORDER BY TRANSCRIPT.YEAR, TRANSCRIPT.SEMESTER, COURSE.CRSCODE");
     oci_execute($stmt, OCI_DEFAULT);
     while (oci_fetch($stmt)) {# use upper-case column names in oci_result()
        echo " <tr><td><a href='courseinfo.php?crscode=" . oci_result($stmt, "CRSCODE") . "&amp;submit=submit&amp;_submit_check=1'> " . oci_result($stmt, "CRSCODE") . "</a></td><td>" . oci_result($stmt, "CRSNAME") . "</td><td>" . oci_result($stmt, "SEMESTER") . "</td><td>" . oci_result($stmt, "YEAR") . "</td><td>" . oci_result($stmt, "GRADE") . "</td></tr>\n";
     }

     // Commit transaction...
     oci_commit($conn);

     // cleanup...
     oci_free_statement($stmt);
     oci_close($conn);
     
   } else{
     // logon error
     $err = oci_error();
     echo "Oracle Connect Error: " . $err['message'];
   }
?>
</table>
<? require "inc/loggedinfooter.php"; ?>
