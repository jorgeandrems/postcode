#include<stdlib.h>
#include<stdio.h>

struct List {
   int val;
   struct List * next;
};

typedef struct List node;

void main() {
   node * curr, * head;
   int i;

   head = NULL;

   for(i=1;i<=10;i++) {
      curr = (node *)malloc(sizeof(node));
      curr->val = i;
      curr->next  = head;
      head = curr;
   }

   curr = head;

   while(curr) {
      printf("%d\n", curr->val);
      curr = curr->next ;
   }
}

