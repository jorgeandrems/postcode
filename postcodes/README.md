Based on http://download.geonames.org/export/zip/ as documented on http://www.geonames.org/export/ and fairly licensed CC-SA, I have created a postcode indexed array for quick searches of all suburbs based on postcode in AUSTRALIA.

Example
<pre>
"7267": {
    "suburbs": [
    {
        "div_l": "Tasmania",
            "div_s": "TAS",
            "geo_lat": "-41.2571",
            "geo_long": "147.1189",
            "other": "TASMANIA",
            "suburb": "Lalla"
    },
    {
        "div_l": "Tasmania",
        "div_s": "TAS",
        "geo_lat": "-41.25",
        "geo_long": "147.15",
        "other": "TASMANIA",
        "suburb": "Karoola"
    },
    {
        "div_l": "Tasmania",
        "div_s": "TAS",
        "geo_lat": "-41.2167",
        "geo_long": "147.1333",
        "other": "TASMANIA",
        "suburb": "Bangor"
    },
    {
        "div_l": "Tasmania",
        "div_s": "TAS",
        "geo_lat": "-41.2667",
        "geo_long": "147.1333",
        "other": "TASMANIA",
        "suburb": "Turners Marsh"
    }
    ]
},
</pre>


